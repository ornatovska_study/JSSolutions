package com.company;

class ShellSort {
    int [] arr = new int [10];
    public ShellSort(int[] arr) {

        this.arr = arr;
    }


    public static void shellSort(int[] arr) {
        int d = arr.length / 2;
        while (d > 0) {
            for (int i = 0; i < arr.length - d; i++) {
                int j = i;
                while ((j >= 0) && (arr[j] > arr[j + d])) {
                    int temp = arr[j + d];
                    arr[j + d] = arr[j];
                    arr[j] = temp;
                    j--;
                }
            }
            d = d / 2;
        }
        System.out.println();
        System.out.println("Shell Sort");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "  ");
        }
    }

}
