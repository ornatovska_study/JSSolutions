package com.company;

class Cocktail{
        int [] arr = new int [10];
        public Cocktail(int[] arr) {

            this.arr = arr;
        }

        public void cocktailSort(int[] arr){
            int left=0, right=arr.length-1;
            do{
                for (int i=right;i>left;i-- ){
                    if(arr[i-1]>arr[i]){
                        int temp=arr[i];
                        arr[i]=arr[i-1];
                        arr[i-1]=temp;
                    }
                }
                left++;
                for(int i=left;i<right;i++){
                    if(arr[i]>arr[i+1]){
                        int temp=arr[i+1];
                        arr[i+1]=arr[i];
                        arr[i]=temp;
                    }
                }
                right--;
            }while(left<=right);
            System.out.println("Cocktail Sort");

            for (int i=0;i<arr.length;i++){
                System.out.print(arr[i]+ "  ");
            }

        }
}
