package com.company;

import java.util.Scanner;



class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int[] arr = new int[11];
        for (int i = 1; i < arr.length; i++) {
            System.out.println("Введіть " + i + " елемент масиву" );
            arr[i] = input.nextInt();

        }

        System.out.println();
        Cocktail cocktail = new Cocktail(arr);
        cocktail.cocktailSort(arr);
        ShellSort shellSort = new ShellSort(arr);
        shellSort.shellSort(arr);
    }
}
